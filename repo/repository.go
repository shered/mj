package repo

import (
	"context"
	pg "github.com/gobeam/mongo-go-pagination"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

type Repository interface {
	Database() *mongo.Database
	Collection() *mongo.Collection
	FindById(ctx context.Context, id primitive.ObjectID, value interface{}) error
	Create(ctx context.Context, model interface{}) (primitive.ObjectID, error)
	CreateMany(ctx context.Context, models []interface{}) ([]primitive.ObjectID, error)
	Update(ctx context.Context, model interface{}, id primitive.ObjectID, dateModified int64) error
	Delete(ctx context.Context, id []primitive.ObjectID) error
	Filter(ctx context.Context, filters map[string]interface{}, sortFields []string, sortValues []int, page int64, limit int64, result interface{}) (int64, error)
	CreateCompoundIndex(ctx context.Context, compoundIndex []string, unique bool) error
	FindOne(ctx context.Context, filters map[string]interface{}, value interface{}) error
	Aggregate(ctx context.Context, sortFields []string, sortValues []int, page int64, limit int64, agg ...interface{}) ([]interface{}, int64, error)
	WithTransaction(ctx context.Context, fn func(sessCtx mongo.SessionContext) (interface{}, error), opts ...*options.TransactionOptions) (interface{}, error)
}

type BaseRepository struct {
	CollectionName string
	DB             *mongo.Database
}

func (b *BaseRepository) Database() *mongo.Database {
	return b.DB
}

func (b *BaseRepository) Collection() *mongo.Collection {
	return b.DB.Collection(b.CollectionName)
}

func (b *BaseRepository) FindById(ctx context.Context, id primitive.ObjectID, value interface{}) error {
	res := b.DB.Collection(b.CollectionName).FindOne(ctx, bson.D{{"_id", id}})
	if res.Err() != nil {
		return res.Err()
	}
	if err := res.Decode(value); err != nil {
		return err
	}
	return nil
}
func (b *BaseRepository) Create(ctx context.Context, model interface{}) (primitive.ObjectID, error) {
	result, err := b.DB.Collection(b.CollectionName).InsertOne(ctx, model)
	if err != nil {
		return primitive.NilObjectID, err
	}
	if id, ok := result.InsertedID.(primitive.ObjectID); ok {
		return id, nil
	}
	return primitive.NilObjectID, nil
}
func (b *BaseRepository) CreateMany(ctx context.Context, models []interface{}) ([]primitive.ObjectID, error) {
	session, err := b.DB.Client().StartSession()
	if err != nil {
		return nil, err
	}
	defer session.EndSession(ctx)
	callback := func(sessionContext mongo.SessionContext) (interface{}, error) {
		return b.DB.Collection(b.CollectionName).InsertMany(sessionContext, models)
	}
	results, err := session.WithTransaction(ctx, callback)
	if err != nil {
		return nil, err
	}
	var ids []primitive.ObjectID
	if insertResult, ok := results.(*mongo.InsertManyResult); ok {
		for _, result := range insertResult.InsertedIDs {
			if id, ok := result.(primitive.ObjectID); ok {
				ids = append(ids, id)
			}
		}
	}
	return ids, err
}
func (b *BaseRepository) Update(ctx context.Context, model interface{}, id primitive.ObjectID, dateModified int64) error {
	res := b.DB.Collection(b.CollectionName).FindOneAndReplace(
		ctx,
		bson.D{
			{"_id", id},
			{"updated_at", dateModified},
		},
		model,
	)
	return res.Err()
}
func (b *BaseRepository) Delete(ctx context.Context, ids []primitive.ObjectID) error {
	_, err := b.DB.Collection(b.CollectionName).DeleteMany(ctx,
		bson.M{"_id": bson.M{"$in": ids}})
	if err != nil {
		return err
	}
	return nil
}

func (b *BaseRepository) Filter(ctx context.Context, filters map[string]interface{}, sortFields []string, sortValue []int, page int64, limit int64, result interface{}) (int64, error) {
	query := pg.New(b.DB.Collection(b.CollectionName)).Decode(result).Context(ctx).Page(page).Limit(limit)
	if len(sortFields) < 1 {
		query = query.Sort("date_modified", -1)
	} else {
		for i, sort := range sortFields {
			if i < len(sortValue) {
				query = query.Sort(sort, sortValue[i])
			}
		}
	}
	aggPaginatedData, err := query.Filter(filters).Find()
	if err != nil {
		return 0, err
	}
	return aggPaginatedData.Pagination.Total, err
}

func (b *BaseRepository) CreateCompoundIndex(ctx context.Context, compoundIndex []string, unique bool) error {
	var indices bsonx.Doc
	for _, index := range compoundIndex {
		indices = append(indices, bsonx.Elem{
			Key:   index,
			Value: bsonx.Int32(1),
		})
	}
	index := mongo.IndexModel{
		Keys:    indices,
		Options: options.Index().SetUnique(unique),
	}
	_, err := b.DB.Collection(b.CollectionName).Indexes().CreateOne(ctx, index)
	return err
}

func (b *BaseRepository) FindOne(ctx context.Context, filters map[string]interface{}, value interface{}) error {
	res := b.DB.Collection(b.CollectionName).FindOne(ctx, filters)
	if res.Err() != nil {
		return res.Err()
	}
	if err := res.Decode(value); err != nil {
		return err
	}
	return nil
}

func (b *BaseRepository) Aggregate(ctx context.Context, sortFields []string, sortValue []int, page int64, limit int64, agg ...interface{}) ([]interface{}, int64, error) {
	query := pg.New(b.DB.Collection(b.CollectionName)).Context(ctx).Page(page).Limit(limit)
	if len(sortFields) < 1 {
		query = query.Sort("date_modified", -1)
	} else {
		for i, sort := range sortFields {
			if i < len(sortValue) {
				query = query.Sort(sort, sortValue[i])
			}
		}
	}
	aggPaginatedData, err := query.Aggregate(agg...)
	if err != nil {
		return nil, 0, err
	}
	var result []interface{}
	for _, raw := range aggPaginatedData.Data {
		var e map[string]interface{}
		if marshallErr := bson.Unmarshal(raw, &e); marshallErr == nil {
			result = append(result, e)
		}
	}
	return result, aggPaginatedData.Pagination.Total, err
}

func (b *BaseRepository) WithTransaction(ctx context.Context, callback func(sessCtx mongo.SessionContext) (interface{}, error), opts ...*options.TransactionOptions) (interface{}, error) {
	session, err := b.DB.Client().StartSession()
	defer session.EndSession(ctx)
	if err != nil {
		return nil, err
	}
	return session.WithTransaction(ctx, callback, opts...)
}
