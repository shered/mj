package repo

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"sync"
	"time"
)

var (
	lock     sync.RWMutex
	instance *mongo.Database
)

func Init(uri ,db string, maxPool uint64) (*mongo.Database, error) {
	lock.Lock()
	defer lock.Unlock()
	if instance == nil {
		db, err := connectDb(uri, db, maxPool)
		if err != nil {
			return nil, err
		}
		instance = db
	}
	return instance, nil
}

func connectDb(uri ,dbName string, maxPoolSize uint64) (*mongo.Database, error) {
	clientOptions := options.Client().ApplyURI(uri)
	clientOptions.SetMaxPoolSize(maxPoolSize)
	clientOptions.SetMinPoolSize(4)
	clientOptions.SetReadPreference(readpref.Nearest())
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		return nil, err
	}
	ctx, cancel = context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		return nil, err
	}
	return client.Database(dbName), nil
}
