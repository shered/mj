module gitlab.com/shered/mj

go 1.13

require (
	github.com/gobeam/mongo-go-pagination v0.0.6
	github.com/pkg/errors v0.9.1
	go.mongodb.org/mongo-driver v1.7.0
)
