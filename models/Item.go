package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type Item struct {

	// id
	ID_ primitive.ObjectID `json:"id,omitempty" bson:"_id"`

	// by
	// Required: true
	By *string `json:"by"`

	// id
	ID int32 `json:"id,omitempty"`

	// kids
	Kids []int32 `json:"kids"`

	// parent
	Parent int32 `json:"parent,omitempty"`

	// parts
	Parts int32 `json:"parts,omitempty"`

	// score
	Score int32 `json:"score,omitempty"`

	// text
	Text string `json:"text,omitempty"`

	// time
	Time int32 `json:"time,omitempty"`

	// title
	Title string `json:"title,omitempty"`

	// type
	Type string `json:"type,omitempty"`

	// url
	URL string `json:"url,omitempty"`

	// categories
	Categories []string `json:"categories"`

	// text index
	TextIndex string `json:"text_index,omitempty" bson:"text_index"`

	// created at
	CreatedAt int64 `json:"created_at,omitempty" bson:"created_at"`

	// created by
	CreatedBy string `json:"created_by,omitempty" bson:"created_by"`

	// updated at
	UpdatedAt int64 `json:"updated_at,omitempty" bson:"updated_at"`

	// updated by
	UpdatedBy string `json:"updated_by,omitempty" bson:"updated_by"`

	// meta data
	MetaData string `json:"meta_data,omitempty" bson:"meta_data"`

	// cache id
	CacheID string `json:"cache_id,omitempty" bson:"cache_id"`

	NoCategory bool `json:"no_category,omitempty" bson:"no_category"`

	// texts
	Texts []string `json:"texts" bson:"texts"`

}
