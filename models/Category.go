package models

type Category struct {
	Id string `json:"id,omitempty" bson:"_id"`

	Text []string `json:"text,omitempty" bson:"text"`

	// created at
	CreatedAt int64 `json:"created_at,omitempty" bson:"created_at"`

	// created by
	CreatedBy string `json:"created_by,omitempty" bson:"created_by"`

	// updated at
	UpdatedAt int64 `json:"updated_at,omitempty" bson:"updated_at"`

	// updated by
	UpdatedBy string `json:"updated_by,omitempty" bson:"updated_by"`

	// meta data
	MetaData string `json:"meta_data,omitempty" bson:"meta_data"`
}