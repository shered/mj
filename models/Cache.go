package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type Cache struct {

	// id
	ID primitive.ObjectID `json:"id,omitempty" bson:"_id"`

	// created at
	CreatedAt int64 `json:"created_at,omitempty" bson:"created_at"`

	// created by
	CreatedBy string `json:"created_by,omitempty" bson:"created_by"`

	// data
	Data string `json:"data,omitempty" bson:"data"`

	// meta data
	MetaData string `json:"meta_data,omitempty" bson:"meta_data"`

	// updated at
	UpdatedAt int64 `json:"updated_at,omitempty" bson:"updated_at"`

	// updated by
	UpdatedBy string `json:"updated_by,omitempty" bson:"updated_by"`

	// url
	URL string `json:"url,omitempty" bson:"url"`
}
